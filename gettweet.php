<?php

/**
 * Obtain the home_timeline RSS feed using OAuth
 *
 * Although this example uses your user token/secret, you can use
 * the user token/secret of any user who has authorised your application.
 *
 * Instructions:
 * 1) If you don't have one already, create a Twitter application on
 *      https://dev.twitter.com/apps
 * 2) From the application details page copy the consumer key and consumer
 *      secret into the place in this code marked with (YOUR_CONSUMER_KEY
 *      and YOUR_CONSUMER_SECRET)
 * 3) From the application details page copy the access token and access token
 *      secret into the place in this code marked with (A_USER_TOKEN
 *      and A_USER_SECRET)
 * 4) Visit this page using your web browser.
 *
 * @author themattharris
 */
$user = $_GET['username'];
if ($user == "")
	$user = "twitter";

require 'tmhOAuth-master/tmhOAuth.php';
require 'tmhOAuth-master/tmhUtilities.php';
$tmhOAuth = new tmhOAuth(array(
  'consumer_key'    => 'e2EOpKPGRx2XRjdpYBFPA',
  'consumer_secret' => 'PzI8bguMh1ruuZcF5XDXdLzGkGwS0MgwFT2alQDQ',
  'user_token'      => '194496882-JKobe3PbEonYOrNQAa4P225JPZCrAVGclUzzigaT',
  'user_secret'     => 'Hpp0SWnxF4ZhPiYJ7I1aoi6qfz7jgJ7JmgM3FWxKfM',
));

$code = $tmhOAuth->request('GET', $tmhOAuth->url('1/statuses/user_timeline', 'rss'),array('screen_name' => $user));

if ($code == 200) {
  header('Content-Type: application/rss+xml; charset=utf-8');
  echo $tmhOAuth->response['response'];
} else {
  tmhUtilities::pr(htmlentities($tmhOAuth->response['response']));
}